package vn.com.datasection.com.InputOutput;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import vn.com.datasection.InputOutput.ReadFileCsv;
import vn.com.datasection.R.ConnectToR;

public class ReadFileCsvTest {

	

	@Test
	public void testExceptionIsThrown() throws IOException {
		String[] japan = { "ポジ", "中立", "ネガ" };
		String encode = "SHIFT-JIS";
		ReadFileCsv tester = new ReadFileCsv();
		tester.Read("data/reputation01.csv", encode,japan);
	}

	@Test
	public void testReadFile() throws IOException {
		String[] japan = { "ポジ", "中立", "ネガ" };
		String encode = "SHIFT-JIS";
		String[] rowN = { "ブログ" , "プリウス",};
		double[] data = { 2.0, 0.0, 0.0,  0.0, 0.0, 1.0 };
		ConnectToR p = new ConnectToR(data, japan, rowN, 3);
		ReadFileCsv tester = new ReadFileCsv();
		ConnectToR p1=tester.Read("data/reputation01.csv", encode,japan);
		
		for (int i = 0; i < p1.getColName().length; i++) {
			assertEquals(p.getColName()[i],
					p1.getColName()[i]);
		}
		for (int i = 0; i < p1.getRowName().length; i++) {
			assertEquals(p.getRowName()[i],
					p1.getRowName()[i]);
		}
		for (int i = 0; i < data.length; i++) {
			assertEquals(p.getdl()[i]+"",
					p1.getdl()[i]+"");
		}
	}
}
