package vn.com.datasection.com.InputOutput;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import net.arnx.jsonic.JSON;
import net.arnx.jsonic.JSONException;

import org.junit.Test;

import vn.com.datasection.InputOutput.ReadFileCsv;
import vn.com.datasection.InputOutput.WriteJson;
import vn.com.datasection.R.ConnectToR;
import vn.com.datasection.dto.Coord;
import vn.com.datasection.dto.Data;
import vn.com.datasection.dto.Point;


public class WriteJsonTest {

	@Test
	public void testWrite() throws JSONException, FileNotFoundException, IOException {
		Coord dl;
		WriteJson w = new WriteJson();
		String encode = "SHIFT-JIS";
		 String[] japan = { "ポジ", "中立", "ネガ" };
		ReadFileCsv R = new ReadFileCsv();
		ConnectToR p = R.Read("data/reputation03.csv", encode,japan);
		dl = p.getPoint();
		w.write(dl, "data/Point.json");
		List<Point> point = JSON.decode(new FileInputStream("data/Point.json"), List.class);
		String str="[{name=ポジ, type=2, x=-0.705674959450164, y=-0.00413729107030366}, {name=中立, type=2, x=-0.693858671542294, y=1.99706212062182}, {name=ネガ, type=2, x=1.40837277679511, y=-0.0000463170409788788}, {name=ハイブリッドモデル, type=1, x=-0.691710976381453, y=0.148580379975679}, {name=新型, type=1, x=-0.707866011417369, y=-0.0556091408559011}, {name=部品, type=1, x=1.41274563699346, y=-0.000622545238434203}, {name=設計ミス, type=1, x=1.41274563699346, y=-0.000622545238434203}, {name=ブレーキ, type=1, x=-0.707866011417369, y=-0.0556091408559011}, {name=ハイブリッド車, type=1, x=-0.707866011417369, y=-0.0556091408559011}, {name=道筋, type=1, x=-0.707866011417369, y=-0.055609140855901}, {name=気, type=1, x=1.41274563699346, y=-0.000622545238434197}, {name=ミッドランドスクエア, type=1, x=1.41274563699346, y=-0.000622545238434197}, {name=ＩＳシリーズ, type=1, x=-0.707866011417369, y=-0.0556091408559011}]";
		assertEquals(str,point.toString());
		assertEquals(13, point.size());
	}

}
