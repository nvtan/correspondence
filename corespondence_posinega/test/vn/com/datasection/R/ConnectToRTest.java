package vn.com.datasection.R;

import static org.junit.Assert.*;

import java.awt.image.BufferedImage;
import java.io.IOException;
import org.junit.Test;

import vn.com.datasection.dto.Coord;
import vn.com.datasection.dto.Data;
import vn.com.datasection.dto.Point;

public class ConnectToRTest {

	
	@Test
	public void ConnectToRTest() {
		double[] dl={1.0,2.3,3.0,6.0,7.0,4.0};
		String[] colN={"ポジ", "中立", "ネガ"};
		String[] rowN={"ブログ","計画"};
		int colNum=3;
		ConnectToR r = new ConnectToR(dl, colN, rowN, colNum);
		for (int i = 0; i < r.getColName().length; i++) {
			assertEquals(colN[i],
					r.getColName()[i]);
		}
		for (int i = 0; i < r.getRowName().length; i++) {
			assertEquals(rowN[i],
					r.getRowName()[i]);
		}
		for (int i = 0; i < dl.length; i++) {
			assertEquals(dl[i]+"",
					r.getdl()[i]+"");
		}
	}
	
	@Test
	public void showCaChartTest() throws IOException {
		double[] dl={1.0,2.3,3.0,6.0,7.0,4.0,6.0,7.0,4.0};
		String[] colN={"ポジ", "中立", "ネガ"};
		String[] rowN={"ブログ","計画","計画画"};
		int colNum=3;
		ConnectToR r = new ConnectToR(dl, colN, rowN, colNum);
		r.showCaChart();
		assertNotNull(r.getfile());
		
	}
	
	@Test
	public void getAllDataTest() throws IOException {
		
		double[] dl={1.0,2.3,3.0,6.0,7.0,4.0,6.0,7.0,4.0};
		String[] colN={"ポジ", "中立", "ネガ"};
		String[] rowN={"ブログ","計画","計画画"};
		int colNum=3;
		ConnectToR r = new ConnectToR(dl, colN, rowN, colNum);
		Data data=r.getAllData();
		assertNotNull(data);
		assertNotNull(data.getCallData());
		assertNotNull(data.getColData());
		assertNotNull(data.getRowData());
		assertNotNull(data.getEigData());
		assertNotNull(data.getSvdData());
	}
	
	
	@Test
	public void getPointTest() throws IOException {
		
		double[] dl={13.0, 0.1, 0.1, 12.0, 0.0, 0.0, 0.0, 0.0, 9.0,13.0, 0.1, 0.1,};
		String[] colN={"ポジ", "中立", "ネガ"};
		String[] rowN={"ハイブリッドモデル", "新型", "部品", "部品型"};
		int colNum=3;
		
		ConnectToR r = new ConnectToR(dl, colN, rowN, colNum);
		Coord coor=r.getPoint();
		Point[] row=coor.getRow();
		Point[] col=coor.getCol();
		assertNotNull(coor);
		assertEquals(row.length, 4);
		assertEquals(col.length, 3);
	}
	
	@Test
	public void resizeTest() throws IOException {
		BufferedImage img=new BufferedImage(200, 300,1);
		int newW=200;
		int newH=300;

		double[] dl={1.0,2.3,3.0,6.0,7.0,4.0,6.0,7.0,4.0};
		String[] colN={"ポジ", "中立", "ネガ"};
		String[] rowN={"ブログ","計画","計画画"};
		int colNum=3;
		ConnectToR r = new ConnectToR(dl, colN, rowN, colNum);
		BufferedImage image=r.resize(img, newW, newH);
		assertEquals(image.getHeight(), newH);
		assertEquals(image.getWidth(), newW);
		
	}

}
