package vn.com.datasection.dto;

/**
 * @author tan
 * 
 * 			class Coord store Array coordinates of row and column
 * 
 */
public class Coord {
	/**
	 * row is Array coordinates of row
	 * 
	 */
	private Point[] row;
	/**
	 * col is Array coordinates of col
	 * 
	 */
	private Point[] col;

	public Coord(Point[] row, Point[] col) {
		this.col = col;
		this.row = row;
	}

	public Point[] getRowPoint() {
		return row;
	}

	public Point[] getRow() {
		return row;
	}

	public void setRow(Point[] row) {
		this.row = row;
	}

	public Point[] getCol() {
		return col;
	}

	public void setCol(Point[] col) {
		this.col = col;
	}

	public Point[] getColPoint() {
		return col;
	}
}
