package vn.com.datasection.dto;

/**
 * @author tan 
 * EigData is a Array containing all the eigenvalues, the percentage of variance and the cumulative percentage of variance
 */
public class EigData {
	/**
	 * eigenValue is a Array containing all the eigenvalues
	 */
	private double[] eigenValue;
	/**
	 * percenTageOfVariance is a Array containing all the percenTage Of Variance
	 */
	private double[] percenTageOfVariance;
	/**
	 * cumulativePercenTageOfVariance is a Array containing all the cumulative percenTage Of Variance
	 */
	private double[] cumulativePercenTageOfVariance;

	public EigData(double[] eigenValue, double[] percenTageOfVariance,
			double[] cumulativePercenTageOfVariance) {
		this.eigenValue = eigenValue;
		this.percenTageOfVariance = percenTageOfVariance;
		this.cumulativePercenTageOfVariance = cumulativePercenTageOfVariance;

	}

	public double[] getEigenValue() {
		return eigenValue;
	}

	public void setEigenValue(double[] eigenValue) {
		this.eigenValue = eigenValue;
	}

	public void setPercenTageOfVariance(double[] percenTageOfVariance) {
		this.percenTageOfVariance = percenTageOfVariance;
	}

	public void setCumulativePercenTageOfVariance(
			double[] cumulativePercenTageOfVariance) {
		this.cumulativePercenTageOfVariance = cumulativePercenTageOfVariance;
	}

	public double[] getPercenTageOfVariance() {
		return percenTageOfVariance;
	}

	public double[] getCumulativePercenTageOfVariance() {
		return cumulativePercenTageOfVariance;
	}

}
