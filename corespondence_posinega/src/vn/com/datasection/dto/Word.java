package vn.com.datasection.dto;

/**
 * @author tan
 * 
 * class Word is store a word and value positive,neutral and negative of it
 *
 */
public class Word {
	/**
	 * Name of word	 
	 */
	private String word;
	/**
	 * 	 posinega is store value positive,neutral and negative of word
	 */
	private double[] posinega = new double[3];
	/**
	 * 	 total is total value positive+neutral+negative of word
	 */
	private double total;

	public boolean equals(Word i) {
		return i.word.equalsIgnoreCase(this.word);
	}

	public Word(String word, double i, int addres) {
		this.word = word;
		posinega[addres] = i;
		total = i;
	}

	public double[] getPosinega() {
		return posinega;
	}

	public void setPosinega(double[] posinega) {
		this.posinega = posinega;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Word addPos(double i, int addres) {
		posinega[addres] = i;
		total += i;
		return this;
	}

	public String getWord() {
		return word;
	}

	public double[] getPos() {
		return posinega;
	}

	public double getTotal() {
		return total;
	}
}
