package vn.com.datasection.dto;

/**
 * @author tan
 *
 */
public class SvdData {
	/**
	 *vs is 
	 */
	private double[] vs;
	/**
	 *U is Array coordinates of column before correspondence 
	 */
	private double[] U;
	/**
	 *V is Array coordinates of row before correspondence 
	 */
	private double[] V;

	public SvdData(double[] vs, double[] U, double[] V) {
		this.vs = vs;
		this.U = U;
		this.V = V;
	}

	public double[] getVs() {
		return vs;
	}

	public double[] getU() {
		return U;
	}

	public void setVs(double[] vs) {
		this.vs = vs;
	}

	public void setU(double[] u) {
		U = u;
	}

	public void setV(double[] v) {
		V = v;
	}

	public double[] getV() {
		return V;
	}
}
