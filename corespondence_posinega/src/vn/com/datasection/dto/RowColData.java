package vn.com.datasection.dto;

/**
 * @author tan
 * a list of Array with all the results for the column and row variable
 *
 */
public class RowColData {
	/**
	 *coord is a list of Array coordinates of row and column to draw
	 */
	private double[] coord;
	/**
	 *contrib is contribute of this point 
	 */
	private double[] contrib;
	/**
	 *cos2 is value cos*cos  of this point 
	 */
	private double[] cos2;
	/**
	 *inertia is value inertia  of this point 
	 */
	private double[] inertia;

	public RowColData(double[] coord, double[] contrib, double[] cos2,
			double[] inertia) {
		this.coord = coord;
		this.contrib = contrib;
		this.cos2 = cos2;
		this.inertia = inertia;
	}

	public double[] getCoord() {
		return coord;
	}

	public void setCoord(double[] coord) {
		this.coord = coord;
	}

	public void setContrib(double[] contrib) {
		this.contrib = contrib;
	}

	public void setCos2(double[] cos2) {
		this.cos2 = cos2;
	}

	public void setInertia(double[] inertia) {
		this.inertia = inertia;
	}

	public double[] getContrib() {
		return contrib;
	}

	public double[] getCos2() {
		return cos2;
	}

	public double[] getInertia() {
		return inertia;
	}

}
