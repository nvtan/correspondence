package vn.com.datasection.dto;

/**
 * @author tan
 *
 *class Data is store all data after correspondence
 */
public class Data {
	private CallData call;
	private EigData eig;
	private RowColData row, col;
	private SvdData svd;

	public Data(CallData call, EigData eig, RowColData row, RowColData col,
			SvdData svd) {
		this.call = call;
		this.eig = eig;
		this.row = row;
		this.col = col;
		this.svd = svd;
	}
	public void setCall(CallData call) {
		this.call = call;
	}
	public void setEig(EigData eig) {
		this.eig = eig;
	}
	public void setRow(RowColData row) {
		this.row = row;
	}
	public void setCol(RowColData col) {
		this.col = col;
	}
	public void setSvd(SvdData svd) {
		this.svd = svd;
	}

	public CallData getCallData() {
		return call;
	}

	public EigData getEigData() {
		return eig;
	}

	public RowColData getRowData() {
		return row;
	}

	public RowColData getColData() {
		return col;
	}

	public SvdData getSvdData() {
		return svd;
	}
}
