package vn.com.datasection.dto;

/**
 * @author tannv
 * 
 *         a list with some statistics
 * 
 */
public class CallData {
	/**
	 * marge_Col is margin of ArrayList Column
	 */
	private double[] marge_Col;
	/**
	 * marge_Row is margin of ArrayList Row
	 */
	private double[] marge_Row;
	/**
	 * number dimension to display
	 */
	private double[] ncp;
	/**
	 * all is 1
	 */
	private double[] row_W;

	public CallData(double[] marge_Col, double[] marge_Row, double[] ncp,
			double[] row_W) {
		this.marge_Col = marge_Col;
		this.marge_Row = marge_Row;
		this.ncp = ncp;
		this.row_W = row_W;
	}

	public double[] getMarge_Col() {
		return marge_Col;
	}

	public void setMarge_Col(double[] marge_Col) {
		this.marge_Col = marge_Col;
	}

	public void setMarge_Row(double[] marge_Row) {
		this.marge_Row = marge_Row;
	}

	public void setNcp(double[] ncp) {
		this.ncp = ncp;
	}

	public void setRow_W(double[] row_W) {
		this.row_W = row_W;
	}

	public double[] getMarge_Row() {
		return marge_Row;
	}

	public double[] getNcp() {
		return ncp;
	}

	public double[] getRow_W() {
		return row_W;
	}

}
