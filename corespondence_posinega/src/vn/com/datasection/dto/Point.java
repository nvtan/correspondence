package vn.com.datasection.dto;

/**
 * @author tan
 *
 *		class Point store word and Coordinates of it
 */
public class Point {
	
	/*
	 *name is Name of word to draw
	 */
	private String name;
	/*
	 * X is Coordinates x of word
	 */
	private double x ;
	/*
	 *Y is Coordinates y of word
	 */
	private double y;
	/*
	 *type is type of word, if word in row then type =1, if word in column then type =2
	 */
	private String type;

	public Point(String name, double x, double y, String type) {
		super();
		this.name = name;
		this.x = x;
		this.y = y;
		this.type = type;
	}

	public String getName() {
		return name;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getX() {
		return x ;
	}
	public double getY() {
		return y ;
	}
	public String gettype() {
		return type ;
	}

	@Override
	public String toString() {
		return "Point [name=" + name + ", x=" + x + ", y=" + y + ", type="
				+ type + "]";
	}
	
}
