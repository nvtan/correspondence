package vn.com.datasection.R;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import rcaller.RCaller;
import rcaller.RCode;
import vn.com.datasection.dto.CallData;
import vn.com.datasection.dto.Coord;
import vn.com.datasection.dto.Data;
import vn.com.datasection.dto.EigData;
import vn.com.datasection.dto.Point;
import vn.com.datasection.dto.RowColData;
import vn.com.datasection.dto.SvdData;

/**
 * @author tannv
 * 
 *         asfsfaa
 */
public class ConnectToR {
/*
 *  caller is to connect to R environment
 */
	private RCaller caller;
	/**
	 * code is store R code
	 */
	private RCode code;
	/**
	 * dl is  Array value posinega of word
	 */
	private double[] dl;
	/**
	 * colName is Name of word in column
	 */
	private String[] colName;
	/**
	 * colName is Name of word in row
	 */
	private String[] rowName;
	/**
	 * colNumber is number column
	 */
	private int colNumber;
	/**
	 * file is file to show image
	 */
	private File file;
	/**
	 * 
	 */
	private Data data;

	/**
	 * 
	 * @param dl is  Array value posinega of word
	 * @param colN is Name of word in column
	 * @param rowN is Name of word in row
	 * @param colNum is number column
	 */
	public ConnectToR(double[] dl, String[] colN, String[] rowN, int colNum) {
		caller = new RCaller();
		code = new RCode();
		this.dl = dl;
		colName = colN;
		rowName = rowN;
		colNumber = colNum;
		for (int i = 0; i < colNumber; i++) {
			if (dl[i] == 0.0)
				dl[i] += 0.1;
		}
		caller.setRscriptExecutable("/usr/bin/Rscript");
		caller.cleanRCode();

		code.addDoubleArray("data", this.dl);
		code.addStringArray("colname", colName);
		code.addStringArray("rowname", rowName);

		code.addRCode("example<-matrix(data,ncol=" + this.colNumber
				+ ",byrow=TRUE)");
		code.addRCode("colnames(example)<-colname");
		code.addRCode("rownames(example)<-rowname");
		caller.setRCode(code);
		caller.getRCode().getCode().toString();

	}
	

	// show CaChart
	public void showCaChart() throws IOException {
		code.addRCode("library(FactoMineR)");
		code.addRCode("res.ca <- CA(example,graph=FALSE)");
		file = code.startPlot();
		code.addRCode("plot(res.ca,title=\"Corespondence PosiNega\",autoLab=\"yes\")");
		code.endPlot();
		caller.setRCode(code);
		caller.runOnly();
		showImage();
	}

	public Data getAllData() throws IOException {
		code.addRCode("library(FactoMineR)");
		code.addRCode("res.ca <- CA(example,graph=FALSE)");
		code.addRCode("svd<-res.ca[[\"svd\"]]");
		code.addRCode("call<-res.ca[[\"call\"]]");
		code.addRCode("col<-res.ca[[\"col\"]]");
		code.addRCode("eig<-res.ca[[\"eig\"]]");
		code.addRCode("row<-res.ca[[\"row\"]]");
		caller.setRCode(code);
		caller.runAndReturnResult("row");
		RowColData row = new RowColData(caller.getParser().getAsDoubleArray(
				"coord"), caller.getParser().getAsDoubleArray("contrib"),
				caller.getParser().getAsDoubleArray("cos2"), caller.getParser()
						.getAsDoubleArray("inertia"));
		caller.runAndReturnResult("col");
		RowColData col = new RowColData(caller.getParser().getAsDoubleArray(
				"coord"), caller.getParser().getAsDoubleArray("contrib"),
				caller.getParser().getAsDoubleArray("cos2"), caller.getParser()
						.getAsDoubleArray("inertia"));
		caller.runAndReturnResult("eig");
		EigData eig = new EigData(caller.getParser().getAsDoubleArray(
				"eigenvalue"), caller.getParser().getAsDoubleArray(
				"percentageofvariance"), caller.getParser().getAsDoubleArray(
				"cumulativepercentageofvariance"));
		caller.runAndReturnResult("call");
		CallData call = new CallData(caller.getParser().getAsDoubleArray(
				"marge_col"), caller.getParser().getAsDoubleArray("marge_row"),
				caller.getParser().getAsDoubleArray("ncp"), caller.getParser()
						.getAsDoubleArray("row_w"));
		caller.runAndReturnResult("svd");
		SvdData svd = new SvdData(caller.getParser().getAsDoubleArray("vs"),
				caller.getParser().getAsDoubleArray("U"), caller.getParser()
						.getAsDoubleArray("U"));
		data = new Data(call, eig, row, col, svd);
		return data;
	}

	public Coord getPoint() {
		double[] rowData = new double[rowName.length * 2];
		double[] colData = new double[colName.length * 2];
		Point[] row = new Point[rowName.length];
		Point[] col = new Point[colName.length];
		code.addRCode("library(FactoMineR)");
		code.addRCode("res.ca <- CA(example,graph=FALSE)");
		code.addRCode("col<-res.ca[[\"col\"]]");
		code.addRCode("row<-res.ca[[\"row\"]]");
		caller.setRCode(code);
		caller.runAndReturnResult("row");
		rowData = caller.getParser().getAsDoubleArray("coord");
		for (int i = 0; i < rowName.length; i++) {
			Point p = new Point(rowName[i], rowData[i], rowData[i
					+ rowName.length], "1");
			row[i] = p;
		}
		caller.runAndReturnResult("col");
		colData = caller.getParser().getAsDoubleArray("coord");
		for (int i = 0; i < colName.length; i++) {
			Point p = new Point(colName[i], colData[i], colData[i
					+ colName.length], "2");
			col[i] = p;
		}
		Coord data = new Coord(row, col);
		return data;

	}

	// Save CaChart To File
	public void saveCaChartToFile(String file1) throws IOException {
		ImageIcon images = code.getPlot(file);
		Image i = images.getImage();
		BufferedImage ima = new BufferedImage(i.getWidth(null),
				i.getHeight(null), BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = ima.createGraphics();
		g2.drawImage(i, 0, 0, i.getWidth(null), i.getHeight(null), null);
		g2.dispose();
		ImageIO.write(ima, "jpg", new File(file1));

	}

	// Show Image
	public void showImage() {
		ImageIcon images = code.getPlot(file);
		Image i = images.getImage();
		BufferedImage ima = new BufferedImage(i.getWidth(null),
				i.getHeight(null), BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = ima.createGraphics();
		g2.drawImage(i, 0, 0, i.getWidth(null), i.getHeight(null), null);
		g2.dispose();
		BufferedImage image = resize(ima, 750, 750);
		ImageIcon im = new ImageIcon();
		im.setImage(image);
		JFrame frame = new JFrame("My GUI");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 800);
		frame.setResizable(true);
		frame.setLocationRelativeTo(null);

		// Inserts the image icon

		JLabel label1 = new JLabel(" ", im, JLabel.CENTER);
		frame.getContentPane().add(label1);

		frame.validate();
		frame.setVisible(true);

	}

	// Zoom Image
	public BufferedImage resize(BufferedImage img, int newW, int newH) {
		int w = img.getWidth();
		int h = img.getHeight();
		BufferedImage dimg = new BufferedImage(newW, newH, img.getType());
		Graphics2D g = dimg.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
				RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.drawImage(img, 0, 0, newW, newH, 0, 0, w, h, null);
		g.dispose();
		return dimg;
	}

/**
 * 
 * Set and Get function is only for Test 
 */
	public RCaller getCaller() {
		return caller;
	}


	public void setCaller(RCaller caller) {
		this.caller = caller;
	}


	public RCode getCode() {
		return code;
	}


	public void setCode(RCode code) {
		this.code = code;
	}


	public double[] getdl() {
		return dl;
	}


	public void setdl(double[] dl) {
		this.dl = dl;
	}


	public String[] getColName() {
		return colName;
	}


	public void setColName(String[] colName) {
		this.colName = colName;
	}


	public String[] getRowName() {
		return rowName;
	}


	public void setRowName(String[] rowName) {
		this.rowName = rowName;
	}


	public int getColNumber() {
		return colNumber;
	}


	public void setColNumber(int colNumber) {
		this.colNumber = colNumber;
	}


	public File getfile() {
		return file;
	}


	public void setfile(File file) {
		this.file = file;
	}


	public Data getData() {
		return data;
	}


	public void setData(Data data) {
		this.data = data;
	}

}
