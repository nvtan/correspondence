package vn.com.datasection.InputOutput;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JOptionPane;

import vn.com.datasection.R.ConnectToR;
import vn.com.datasection.dto.Word;

/**
 * @author tan
 *Class to read data from File Csv
 */
public class ReadFileCsv {
	/**
	 * keyWordName is List to store List word with vule posinega of this word
	 */
	private List<Word> keyWordName = new ArrayList<Word>();
	/**
	 * sizeRow is number row
	 */
	private  int sizeRow = 10;
	/**
	 * SIZECOL is number column
	 */
	private final int SIZECOL = 3;
	/**
	 * fis is FileInputStream to read file
	 */
	FileInputStream fis = null;
/**
 * 
 * @param file is addres of file csv to read
 * @param encode is encode 
 * @param posiNega is String posinega
 * @return
 * @throws IOException
 */
	public ConnectToR Read(String file,String encode,String[] posiNega) throws IOException {
		try {
			fis = new FileInputStream(new File(file));
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "The source file" + file
					+ " does not exist. ", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(-1);
		}
		BufferedReader in = new BufferedReader(new InputStreamReader(fis,
				encode));
		String str;
		// Read file and store word and value posinega of it to ArrayList.
		// the first line : null
		in.readLine();
		// the second line : "ポジ", "中立", "ネガ"
		in.readLine();
		// the third line :
		in.readLine();
		while ((str = in.readLine()) != null) {
			String[] word = str.split(",");
			if (word.length > 0 && !word[0].equals("")) {
				addWord(word[0], Double.parseDouble(word[2]), 0);
			}
			if (word.length > 3 && !word[3].equals("")) {
				addWord(word[3], Double.parseDouble(word[5]), 1);
			}
			if (word.length > 6 && !word[6].equals("")) {
				addWord(word[6], Double.parseDouble(word[8]), 2);
			}
		}
		in.close();
		// sord ArrayList keyWordName
		Collections.sort(keyWordName, new Comparator<Word>() {
			@Override
			public int compare(Word t, Word t1) {
				if (t.getTotal() > t1.getTotal()) {
					return -1;
				} else if (t.getTotal() == t1.getTotal()) {
					return 0;
				} else
					return 1;
			}
		});

		if (sizeRow > keyWordName.size())
			sizeRow = keyWordName.size();
		String[] keyWord = new String[sizeRow];
		double[] data = new double[sizeRow * SIZECOL];
		// get sizeRow word  have biggest value total
		int i = 0;
		while (i < sizeRow) {
			keyWord[i] = keyWordName.get(i).getWord();
			data[i * 3] = keyWordName.get(i).getPos()[0];
			data[i * 3 + 1] = keyWordName.get(i).getPos()[1];
			data[i * 3 + 2] = keyWordName.get(i).getPos()[2];
			i++;
		}
		ConnectToR p = new ConnectToR(data, posiNega, keyWord, SIZECOL);
		return p;
	}
	// add 1 word with value posinega of it to ArrayList
	private void addWord(String word, double i, int addres) {
		Word w = new Word(word, i, addres);
		for (Word v : keyWordName) {
			if (w.equals(v)) {
				keyWordName.remove(v);
				keyWordName.add(v.addPos(i, addres));
				return;
			}
		}
		keyWordName.add(w);

	}
	
	/*
	 * Set and Get function is only for Test
	 */
	public List<Word> getKeyWordName() {
		return keyWordName;
	}
	public void setKeyWordName(List<Word> keyWordName) {
		this.keyWordName = keyWordName;
	}
	public int getSizeRow() {
		return sizeRow;
	}
	public void setSizeRow(int sizeRow) {
		this.sizeRow = sizeRow;
	}
	public FileInputStream getFis() {
		return fis;
	}
	public void setFis(FileInputStream fis) {
		this.fis = fis;
	}
	public int getSIZECOL() {
		return SIZECOL;
	}
}
