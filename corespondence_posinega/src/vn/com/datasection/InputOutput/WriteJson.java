package vn.com.datasection.InputOutput;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.arnx.jsonic.JSON;
import net.arnx.jsonic.JSONException;
import vn.com.datasection.dto.Coord;
import vn.com.datasection.dto.Point;
public class WriteJson {

	/**
	 * @param dl is  Array coordinates of row and column
	 * @param address is address to save file json
	 * @throws JSONException
	 * @throws FileNotFoundException
	 * @throws IOException
	 * 
	 *             fgdsfd
	 */
	public void write(Coord dl, String address) throws JSONException,
			FileNotFoundException, IOException {
		Point[] row, col;
		col = dl.getColPoint();
		row = dl.getRowPoint();
		List<Point> point = new ArrayList<Point>();
		for (int i = 0; i < col.length; i++) {
			point.add(col[i]);
		}

		for (int i = 0; i < row.length; i++) {
			point.add(row[i]);
		}

		JSON.encode(point, new FileOutputStream(address));
	}
}
