package vn.com.datasection.execute;

import java.io.IOException;

import vn.com.datasection.InputOutput.ReadFileCsv;
import vn.com.datasection.R.ConnectToR;
import vn.com.datasection.InputOutput.*;
import vn.com.datasection.dto.Coord;
import vn.com.datasection.dto.Data;

public class Corespondence {
	
	public static void main(String[] args) throws IOException {
		Data data;
		Coord dl;
		WriteJson w = new WriteJson();
		String encode = "SHIFT-JIS";
		 String[] japan = { "ポジ", "中立", "ネガ" };
		ReadFileCsv R = new ReadFileCsv();
		// TODO Auto-generated method stub
		ConnectToR p = R.Read("data/reputation03.csv", encode,japan);
		dl = p.getPoint();
		w.write(dl, "data/Point.json");
		
	}

}
